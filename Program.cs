﻿using System;

namespace DigitDegree
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(digitDegree(5));
        }

        //the number of times we need to replace n with the sum of its digits until we get one digit

        static int digitDegree(int n) {
            int count = 0;
    
            while(n > 9){
                int total = 0;
                while(n > 0){
                    total += n % 10;
                    n /= 10;
                }
                n = total;
                count++;
            }
            return count++;
        }

    }
}
